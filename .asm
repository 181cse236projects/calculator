# Add any directories, files, or patterns you don't want to be tracked by version control

              
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h  
.model small

.stack 100h
.data

p1 db 0
p2 db 0

p3 db 0 
p4 db 0 
p5 db 0 
p6 db 0 
p7 db 0 
p8 db 0 
p9 db 0 
p0 db 0 
 
r db 0

 
xmin dw 30
xmax dw 330          ;big box draw
ymin dw 20
ymax dw 245 

xmin1 dw 40
xmax1 dw 320   ;display box draw
ymin1 dw 40
ymax1 dw 85 

xmin3 dw 40
xmax3 dw 112      ;1st small box draw
ymin3 dw 90
ymax3 dw 120


xmin2 dw 118      ;2nd
xmax2 dw 190  
ymin2 dw 90
ymax2 dw 120  

xmin4 dw 196
xmax4 dw 268    ;3rd
ymin4 dw 90
ymax4 dw 120 

xmin5 dw 274
xmax5 dw 316   ;4th
ymin5 dw 90
ymax5 dw 120  

xmin6 dw 40      ;5th
xmax6 dw 112  
ymin6 dw 125
ymax6 dw 155

xmin7 dw 118
xmax7 dw 190      ;6th
ymin7 dw 125
ymax7 dw 155

xmin8 dw 196
xmax8 dw 268      ;7th
ymin8 dw 125
ymax8 dw 155

xmin9 dw 274
xmax9 dw 316   ;8th
ymin9 dw 125
ymax9 dw 155

xmin10 dw 40       ;9th
xmax10 dw 112  
ymin10 dw 160
ymax10 dw 190

xmin11 dw 118
xmax11 dw 190      ;10th
ymin11 dw 160
ymax11 dw 190

xmin12 dw 196
xmax12 dw 268       ;11th
ymin12 dw 160
ymax12 dw 190

xmin13 dw 274
xmax13 dw 316    ;12th
ymin13 dw 160
ymax13 dw 190

xmin14 dw 40
xmax14 dw 112    ;13th
ymin14 dw 195
ymax14 dw 225

xmin15 dw 118
xmax15 dw 190      ; 14th
ymin15 dw 195
ymax15 dw 225  

xmin17 dw 196
xmax17 dw 268       ;15th
ymin17 dw 195
ymax17 dw 225

xmin16 dw 274
xmax16 dw 316       ;16th
ymin16 dw 195
ymax16 dw 225


.code

;........print function


 PUSHALL MACRO
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
ENDM  

POPALL MACRO
    POP DX
    POP CX
    POP BX
    POP AX
ENDM


print macro x, y, attrib, sdat
LOCAL   s_dcl, skip_dcl, s_dcl_end
    PUSHALL
    mov dx, cs
    mov es, dx
    mov ah, 13h
    mov al, 1
    mov bh, 0
    mov bl, attrib
    mov cx, offset s_dcl_end - offset s_dcl
    mov dl, x
    mov dh, y
    mov bp, offset s_dcl
    int 10h
    POPALL
    jmp skip_dcl
    s_dcl DB sdat
    s_dcl_end DB 0
    skip_dcl:
        
endm  





proc main
    mov dx,@data
    mov ds,dx
    
    
    mov p0,0
    mov p1,0
    mov p2,0
    mov p3,0   
    mov p4,0
    mov p5,0
    mov p6,0
    mov p7,0
    mov p8,0
    mov p9,0
    
    ;graphics mode setup
    mov ah,0
    mov al,12h
    int 10h
    ;mouse setup
    mov ax,0
    int 33h
    
    mov ax,3
    int 33h
  
    
  print 8,24,0000_1111b,"7"  
  print 19,24,0000_1111b,"8" 
  print 27,24,0000_1111b,"9" 
  print 37,24,0000_1111b,"+" 
  
;  print 8,35,0000_1111b,"4"  
;  print 19,35,0000_1111b,"5" 
;  print 27,35,0000_1111b,"6" 
;  print 37,35,0000_1111b,"-" 
;  print 8,45,0000_1111b,"1"  
;  print 19,45,0000_1111b,"2" 
;  print 27,45,0000_1111b,"3" 
;  print 37,45,0000_1111b,"*"
;  print 8,55,0000_1111b,"A/C"  
;  print 19,55,0000_1111b,"0" 
;  print 27,55,0000_1111b,"=" 
;  print 37,55,0000_1111b,"/" 
;     
;   print 148,104,0000_1111b,"7"  
;  print 306,104,0000_1111b,"8" 
;  print 462,104,0000_1111b,"9" 
;  print 590,104,0000_1111b,"+"         
    
    
mov cx,xmin
 L1:
   mov ah,0ch
   mov al,13
   mov dx,20
   int 10h
   inc cx
   cmp cx,xmax
   jle L1    

mov dx,ymin
 L2:
      
   mov ah,0ch
   mov al,13
   mov cx,30
   int 10h
   inc dx
   cmp dx,ymax
   jle L2    

mov cx,xmin
 L3:
   mov ah,0ch
   mov al,13
   mov dx,245
   int 10h
   inc cx
   cmp cx,xmax
   jle L3 
   
mov dx,ymin
 L4:
      
   mov ah,0ch
   mov al,13
   mov cx,330
   int 10h
   inc dx
   cmp dx,ymax
   jle L4 
   
mov cx,xmin1
 L5:
   mov ah,0ch
   mov al,13
   mov dx,40
   int 10h
   inc cx
   cmp cx,xmax1
   jle L5    

mov dx,ymin1
 L6:
      
   mov ah,0ch
   mov al,13
   mov cx,40
   int 10h
   inc dx
   cmp dx,ymax1
   jle L6    

mov cx,xmin1
 L7:
   mov ah,0ch
   mov al,13
   mov dx,85
   int 10h
   inc cx
   cmp cx,xmax1
   jle L7
   
mov dx,ymin1
 L8:
      
   mov ah,0ch
   mov al,13
   mov cx,320
   int 10h
   inc dx
   cmp dx,ymax1
   jle L8           
                                                                                       
mov cx,xmin2
 L9:
   mov ah,0ch
   mov al,13
   mov dx,90
   int 10h
   inc cx
   cmp cx,xmax2
   jle L9    

mov dx,ymin2
 L10:
      
   mov ah,0ch
   mov al,13
   mov cx,118
   int 10h
   inc dx
   cmp dx,ymax2
   jle L10    

mov cx,xmin2
 L11:
   mov ah,0ch
   mov al,13
   mov dx,120
   int 10h
   inc cx
   cmp cx,xmax2
   jle L11 
   
mov dx,ymin2
 L12:
      
   mov ah,0ch
   mov al,13
   mov cx,190
   int 10h
   inc dx
   cmp dx,ymax2
   jle L12 
   
mov cx,xmin3
 L13:
   mov ah,0ch
   mov al,13
   mov dx,90
   int 10h
   inc cx
   cmp cx,xmax3
   jle L13    

mov dx,ymin3
 L14:
      
   mov ah,0ch
   mov al,13
   mov cx,40
   int 10h
   inc dx
   cmp dx,ymax3
   jle L14    

mov cx,xmin3
 L15:
   mov ah,0ch
   mov al,13
   mov dx,120
   int 10h
   inc cx
   cmp cx,xmax3
   jle L15 
   
mov dx,ymin3
 L16:
      
   mov ah,0ch
   mov al,13
   mov cx,112
   int 10h
   inc dx
   cmp dx,ymax3
   jle L16 
           
mov cx,xmin4
 L17:
   mov ah,0ch
   mov al,13
   mov dx,90
   int 10h
   inc cx
   cmp cx,xmax4
   jle L17    

mov dx,ymin4
 L18:                                      
   mov ah,0ch
   mov al,13
   mov cx,196
   int 10h
   inc dx
   cmp dx,ymax4
   jle L18   

mov cx,xmin4
 L19:
   mov ah,0ch
   mov al,13
   mov dx,120
   int 10h
   inc cx
   cmp cx,xmax4
   jle L19 
   
mov dx,ymin4
 L20:
      
   mov ah,0ch
   mov al,13
   mov cx,268
   int 10h
   inc dx
   cmp dx,ymax4
   jle L20 
   
mov cx,xmin5
 L21:
   mov ah,0ch
   mov al,13
   mov dx,90
   int 10h
   inc cx
   cmp cx,xmax5
   jle L21    

mov dx,ymin5
 L22:
      
   mov ah,0ch
   mov al,13
   mov cx,274
   int 10h
   inc dx
   cmp dx,ymax5
   jle L22    

mov cx,xmin5
 L23:
   mov ah,0ch
   mov al,13
   mov dx,120
   int 10h
   inc cx
   cmp cx,xmax5
   jle L23 
   
mov dx,ymin5
 L24:
      
   mov ah,0ch
   mov al,13
   mov cx,316
   int 10h
   inc dx
   cmp dx,ymax5
   jle L24 
               

   cmp cx,xmax12
   jle L49           
mov cx,xmin6
 L25:
   mov ah,0ch
   mov al,13
   mov dx,125
   int 10h
   inc cx
   cmp cx,xmax6
   jle L25    

mov dx,ymin6
 L26:
      
   mov ah,0ch
   mov al,13
   mov cx,40
   int 10h
   inc dx
   cmp dx,ymax6
   jle L26    

mov cx,xmin6
 L27:
   mov ah,0ch
   mov al,13
   mov dx,155
   int 10h
   inc cx
   cmp cx,xmax6
   jle L27 
   
mov dx,ymin6
 L28:
      
   mov ah,0ch
   mov al,13
   mov cx,112
   int 10h
   inc dx
   cmp dx,ymax6
   jle L28
mov cx,xmin7
 L29:
   mov ah,0ch
   mov al,13
   mov dx,125
   int 10h
   inc cx
   cmp cx,xmax7
   jle L29    

mov dx,ymin7
 L30:
      
   mov ah,0ch
   mov al,13
   mov cx,118
   int 10h
   inc dx
   cmp dx,ymax7
   jle L30    

mov cx,xmin7
 L31:
   mov ah,0ch
   mov al,13
   mov dx,155
   int 10h
   inc cx
   cmp cx,xmax7
   jle L31 
   
mov dx,ymin7
 L32:
      
   mov ah,0ch
   mov al,13
   mov cx,190
   int 10h
   inc dx
   cmp dx,ymax7
   jle L32 
mov cx,xmin8
 L33:
   mov ah,0ch
   mov al,13
   mov dx,125
   int 10h
   inc cx
   cmp cx,xmax8
   jle L33    

mov dx,ymin8
 L34:
      
   mov ah,0ch
   mov al,13
   mov cx,196
   int 10h
   inc dx
   cmp dx,ymax8
   jle L34    

mov cx,xmin8
 L35:
   mov ah,0ch
   mov al,13
   mov dx,155
   int 10h
   inc cx
   cmp cx,xmax8
   jle L35 
   
mov dx,ymin8
 L36:
      
   mov ah,0ch
   mov al,13
   mov cx,268
   int 10h
   inc dx
   cmp dx,ymax8
   jle L36 
mov cx,xmin9
 L37:
   mov ah,0ch
   mov al,13
   mov dx,125
   int 10h
   inc cx
   cmp cx,xmax9
   jle L37    

mov dx,ymin9
 L38:
      
   mov ah,0ch
   mov al,13
   mov cx,274
   int 10h
   inc dx
   cmp dx,ymax9
   jle L38    

mov cx,xmin9
 L39:
   mov ah,0ch
   mov al,13
   mov dx,155
   int 10h
   inc cx
   cmp cx,xmax9
   jle L39 
   
mov dx,ymin9
 L40:
      
   mov ah,0ch
   mov al,13
   mov cx,316
   int 10h
   inc dx
   cmp dx,ymax9
   jle L40 

mov cx,xmin10
 L41:
   mov ah,0ch
   mov al,13
   mov dx,160
   int 10h
   inc cx
   cmp cx,xmax10
   jle L41   

mov dx,ymin10
 L42:
      
   mov ah,0ch
   mov al,13
   mov cx,40
   int 10h
   inc dx
   cmp dx,ymax10
   jle L42    

mov cx,xmin10
 L43:
   mov ah,0ch
   mov al,13
   mov dx,190
   int 10h
   inc cx
   cmp cx,xmax10
   jle L43 
   
mov dx,ymin10
 L44:
      
   mov ah,0ch
   mov al,13
   mov cx,112
   int 10h
   inc dx
   cmp dx,ymax10
   jle L44
   
mov cx,xmin11
 L45:
   mov ah,0ch
   mov al,13
   mov dx,160
   int 10h
   inc cx
   cmp cx,xmax11
   jle L45    

mov dx,ymin11
 L46:
      
   mov ah,0ch
   mov al,13
   mov cx,118
   int 10h
   inc dx
   cmp dx,ymax11
   jle L46    

mov cx,xmin11
 L47:
   mov ah,0ch
   mov al,13
   mov dx,190
   int 10h
   inc cx
   cmp cx,xmax11
   jle L47 
   
mov dx,ymin11
 L48:
      
   mov ah,0ch
   mov al,13
   mov cx,190
   int 10h
   inc dx
   cmp dx,ymax11
   jle L48                     

mov cx,xmin12
 L49:
   mov ah,0ch
   mov al,13
   mov dx,160
   int 10h
   inc cx 
   cmp cx,xmax12
   jle L49

mov dx,ymin12
 L50:
      
   mov ah,0ch
   mov al,13
   mov cx,196
   int 10h
   inc dx
   cmp dx,ymax12
   jle L50    

mov cx,xmin12
 L51:
   mov ah,0ch
   mov al,13
   mov dx,190
   int 10h
   inc cx
   cmp cx,xmax12
   jle L51 
   
mov dx,ymin12
 L52:
      
   mov ah,0ch
   mov al,13
   mov cx,268
   int 10h
   inc dx
   cmp dx,ymax12
   jle L52

mov cx,xmin13
 L53:
   mov ah,0ch
   mov al,13
   mov dx,160
   int 10h
   inc cx
   cmp cx,xmax13
   jle L53    

mov dx,ymin13
 L54:
      
   mov ah,0ch
   mov al,13
   mov cx,274
   int 10h
   inc dx
   cmp dx,ymax13
   jle L54    

mov cx,xmin13
 L55:
   mov ah,0ch
   mov al,13
   mov dx,190
   int 10h
   inc cx
   cmp cx,xmax13
   jle L55 
   
mov dx,ymin13
 L56:
      
   mov ah,0ch
   mov al,13
   mov cx,316
   int 10h
   inc dx
   cmp dx,ymax13
   jle L56          
   
mov cx,xmin14
 L57:
   mov ah,0ch
   mov al,13
   mov dx,195
   int 10h
   inc cx
   cmp cx,xmax14
   jle L57    

mov dx,ymin14
 L58:
      
   mov ah,0ch
   mov al,13
   mov cx,40
   int 10h
   inc dx
   cmp dx,ymax14
   jle L58    

mov cx,xmin14
 L59:
   mov ah,0ch
   mov al,13
   mov dx,225
   int 10h
   inc cx
   cmp cx,xmax14
   jle L59 
   
mov dx,ymin14
 L60:
      
   mov ah,0ch
   mov al,13
   mov cx,112
   int 10h
   inc dx
   cmp dx,ymax14
   jle L60   

mov cx,xmin15
 L61:
   mov ah,0ch
   mov al,13
   mov dx,195
   int 10h
   inc cx
   cmp cx,xmax15
   jle L61    

mov dx,ymin15
 L62:
      
   mov ah,0ch
   mov al,13
   mov cx,118
   int 10h
   inc dx
   cmp dx,ymax15
   jle L62    

mov cx,xmin15
 L63:
   mov ah,0ch
   mov al,13
   mov dx,225
   int 10h
   inc cx
   cmp cx,xmax15
   jle L63 
   
mov dx,ymin15
 L64:
      
   mov ah,0ch
   mov al,13
   mov cx,190
   int 10h
   inc dx
   cmp dx,ymax15
   jle L64 
   
   
mov cx,xmin17
 L69:
   mov ah,0ch
   mov al,13
   mov dx,195
   int 10h
   inc cx
   cmp cx,xmax17
   jle L69   

mov dx,ymin17
 L70:
      
   mov ah,0ch
   mov al,13
   mov cx,196
   int 10h
   inc dx
   cmp dx,ymax17
   jle L70    

mov cx,xmin17
 L71:
   mov ah,0ch
   mov al,13
   mov dx,225
   int 10h
   inc cx
   cmp cx,xmax17
   jle L71 
   
mov dx,ymin17
 L72:
      
   mov ah,0ch
   mov al,13
   mov cx,268
   int 10h
   inc dx
   cmp dx,ymax17
   jle L72

mov cx,xmin16
 L65:
   mov ah,0ch
   mov al,13
   mov dx,195
   int 10h
   inc cx
   cmp cx,xmax16
   jle L65    

mov dx,ymin16
 L66:
      
   mov ah,0ch
   mov al,13
   mov cx,274
   int 10h
   inc dx
   cmp dx,ymax16
   jle L66    

mov cx,xmin16
 L67:
   mov ah,0ch
   mov al,13
   mov dx,225
   int 10h
   inc cx
   cmp cx,xmax16
   jle L67 
   
mov dx,ymin16
 L68:
      
   mov ah,0ch
   mov al,13
   mov cx,316
   int 10h
   inc dx
   cmp dx,ymax16
   jle L68
;
 

;call mouse    ;1st small box click


  cond:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc1
   jmp cond
   
 ;m1:  
 abc1:
   mov cx,80
   cmp cx,80
   jge abc12
   jmp m2
 abc12:
   mov cx,222
   cmp cx,222
   jle abc13
    jmp m2
   
 abc13:
   mov dx,90
   cmp dx,90
   jge abc14
   jmp m2
 abc14:
   mov dx,118
   cmp dx,118 
   
   jle mo1
    jmp m2
   
   mo1:
      print 6,16,0000_1111b,"7"
      
      mov al,"7"
      mov p1,al 
      
     ; jmp cond
                            
;call mouse        ;2nd  

 
   m2:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc2
   jmp m2
   
   
 abc2:
   mov cx,238
   cmp cx,238
   jge abc22 
   jmp m3
 
 abc22:
   mov cx,376
   cmp cx,376
   jle abc23
   jmp m3
 abc23:
   mov dx,90
   cmp dx,90
   jge abc24
   jmp m3
   
 abc24:
   mov dx,118
   cmp dx,118
  jle mo2
  jmp m3 
   mo2:
      print 7,16,0000_1111b,"8" 
      
       mov al,"8"
      mov p2,al
      

;call mouse         ;3rd

      m3:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc3
   jmp m3
   
   
 abc3:
   mov cx,392
   cmp cx,392
   jge abc32 
   jmp m4
     
 abc32:
   mov cx,534
   cmp cx,534
   jle abc33 
   jmp m4
   
 abc33:
   mov dx,90
   cmp dx,90
   jge abc34
   jmp m4
   
 abc34:
   mov dx,118
   cmp dx,118
  jle mo3 
  jmp m4
   
   mo3:
      print 8,16,0000_1111b,"9"
      
       mov al,9
       mov p3,al
 
 
  
;call mouse    
 
 
      m4:              ;....4th
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc4
   jmp m4
   
   
 abc4:
   mov cx,548
   cmp cx,548
   jge abc42
 
 abc42:
   mov cx,628
   cmp cx,628
   jle abc43
   
 abc43:
   mov dx,90
   cmp dx,90
   jge abc44
   
 abc44:
   mov dx,118
   cmp dx,118
   jle mo4
   jmp abc1
   mo4:
     print 9,16,0000_1111b,"+" 
     
     jmp addition
     
 
;call mouse    ...5th

 m5:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc5
   jmp m5
   
   
 abc5:
   mov cx,80
   cmp cx,80
   jge abc52
 
 abc52:
   mov cx,222
   cmp cx,222
   jle abc53
   
 abc53:
   mov dx,125
   cmp dx,125
   jge abc54
   
 abc54:
   mov dx,154
   cmp dx,154
   jle mo5
   
   mo5:
      print 10,16,0000_1111b,"4"    
    
      mov al,"4"
      mov p4,al 
      
            


;call mouse                    ;6th



         m6:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc6
   jmp m6
   
   
 abc6:
   mov cx,238
   cmp cx,238
   jge abc62
 
 abc62:
   mov cx,376
   cmp cx,376
   jle abc63
   
 abc63:
   mov dx,125
   cmp dx,125
   jge abc64
   
 abc64:
   mov dx,154
   cmp dx,154
   jle mo6
   
   mo6:
      print 11,16,0000_1111b,"5" 
   
      mov al,"5"
      mov p5,al 
      

;call mouse    ...7th

  m7:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc7
   jmp m7
   
   
 abc7:
   mov cx,392
   cmp cx,392
   jge abc72
 
 abc72:
   mov cx,534
   cmp cx,534
   jle abc73
   
 abc73:
   mov dx,125
   cmp dx,125
   jge abc74
   
 abc74:
   mov dx,154
   cmp dx,154
   jle mo7
   
   mo7:
      print 12,16,0000_1111b,"6" 
   
      mov al,"6"
      mov p6,al 
      


;call mouse   ....8th


 m8:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc8
   jmp m8
   
   
 abc8:
   mov cx,548
   cmp cx,548
   jge abc82
 
 abc82:
   mov cx,628
   cmp cx,628
   jle abc83
   
 abc83:
   mov dx,125
   cmp dx,125
   jge abc84
   
 abc84:
   mov dx,154
   cmp dx,154
   jle mo8
   
   mo8:
      print 13,16,0000_1111b,"-" 
   
      ; jmp subt
      


 ;call mouse ...9th



 m9:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc9
   jmp m9
   
   
 abc9:
   mov cx,80
   cmp cx,80
   jge abc92
 
 abc92:
   mov cx,222
   cmp cx,222
   jle abc93
   
 abc93:
   mov dx,160
   cmp dx,160
   jge abc94
   
 abc94:
   mov dx,189
   cmp dx,189
   jle mo9
   
   mo9:
      print 14,16,0000_1111b,"1" 
   
      mov al,"1"
      mov p7,al 
      
;call mouse.....10th



 m10:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc10
   jmp m10
   
   
 abc10:
   mov cx,238
   cmp cx,238
   jge abc102
 
 abc102:
   mov cx,376
   cmp cx,376
   jle abc103
   
 abc103:
   mov dx,160
   cmp dx,160
   jge abc104
   
 abc104:
   mov dx,189
   cmp dx,189
   jle mo10
   
   mo10:
      print 15,16,0000_1111b,"2"  

       mov al,"2"
      mov p8,al 
      



;call mouse   .....11th


 m11:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc11
   jmp m10
   
   
 abc11:
   mov cx,392
   cmp cx,392
   jge abc112
 
 abc112:
   mov cx,534
   cmp cx,534
   jle abc113
   
 abc113:
   mov dx,160
   cmp dx,160
   jge abc114
   
 abc114:
   mov dx,189
   cmp dx,189
   jle mo11
   
   mo11:
      print 15,16,0000_1111b,"3" 
 
       mov al,"3"
      mov p9,al 
      

;call mouse  ....12th


  m12:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc121
   jmp m12
   
   
 abc121:
   mov cx,548
   cmp cx,548
   jge abc122
 
 abc122:
   mov cx,628
   cmp cx,628
   jle abc123
   
 abc123:
   mov dx,160
   cmp dx,160
   jge abc124
   
 abc124:
   mov dx,189
   cmp dx,189
   jle mo11
   
   mo12:
      print 16,16,0000_1111b,"*"  


;call mouse ...13th
;
mov xmin3,40

  m13:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc131
   jmp m13
   
   
 abc131:
   mov cx,xmin3
   cmp cx,40
   jge abc132
 
 abc132:
   mov cx,xmax3
   cmp cx,112
   jle abc133
   
 abc133:
   mov dx,ymin3
   cmp dx,90
   jge abc134
   
 abc134:
   mov dx,ymax3
   cmp dx,120
   jle mo13
   
   mo13:
      ;print 17,16,0000_1111b,"A/C"  

;call mouse ......14th

    m14:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc141
   jmp m14
   
   
 abc141:
   mov cx,xmin3
   cmp cx,40
   jge abc142
 
 abc142:
   mov cx,xmax3
   cmp cx,112
   jle abc143
   
 abc143:
   mov dx,ymin3
   cmp dx,90
   jge abc144
   
 abc144:
   mov dx,ymax3
   cmp dx,120
   jle mo14
   
   mo14:
      print 18,16,0000_1111b,"0"  

      mov al,"0"
      mov p0,al 
      


;call mouse ........15th


 m15:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc151
   jmp m15
   
   
 abc151:
   mov cx,xmin3
   cmp cx,40
   jge abc152
 
 abc152:
   mov cx,xmax3
   cmp cx,112
   jle abc153
   
 abc153:
   mov dx,ymin3
   cmp dx,90
   jge abc154
   
 abc154:
   mov dx,ymax3
   cmp dx,120
   jle mo15
   
   mo15:
      print 18,16,0000_1111b,"="  

;call mouse .........16th

       
    m16:
 
   mov ax,3
   int 33h
   
   cmp bx,1
   je abc161
   jmp m16
   
   
 abc161:
   mov cx,xmin3
   cmp cx,40
   jge abc162
 
 abc162:
   mov cx,xmax3
   cmp cx,112
   jle abc153
   
 abc163:
   mov dx,ymin3
   cmp dx,90
   jge abc164
   
 abc164:
   mov dx,ymax3
   cmp dx,120
   jle mo16
   
   mo16:
      print 18,16,0000_1111b,"/"  

  

mov ah,4ch
int 21h  
ret

endp main


addition:
    mov al,p1     ;p1
    
    sub al,48
    
    mov p1,al  ;
    
    mov al,p2     ;p2
    
    sub al,48
    
    mov p2,al
    
    add al,p1   ;p1+p2
         
   ; mov r,al     ;r=p1+p2
;    
    ;mov al,p3      ;p3
;    sub al,48
;;         
;    add al,r
;    
    mov ah,0
    aaa
    
    add ah,48
    add al,48
    
    
    mov bx,ax
    
    
    mov ah,2
    mov dl,bh ;high
    int 21h
    
     
     mov ah,2
     
     mov dl,bl ;low
      int 21h
      
      ret 
      
;subt:
;      
;    mov al,p1     ;p1
;    
;    sub al,48
;    
;    mov p1,al  
;    
;    mov al,p2     ;p2
;    
;    sub al,48
;    
;    ;mov p2,al
;    
;    sub p1,al   ;p1-p2
;         
;    mov al,p1     ;r=p1-p2 
;    
;;    
;    ;mov al,p3      ;p3
;;    sub al,48
;;         
;   ; add al,r
;;    
;    mov ah,0
;    aaa
;    
;    add ah,48
;    add al,48
;    
;    
;    mov bx,ax
;    
;    
;    mov ah,2
;    mov dl,bh ;high
;    int 21h
;    
;     
;     mov ah,2
;     
;     mov dl,bl ;low
;      int 21h
;      
 ;ret
      

end main


    
    
    
    
    


